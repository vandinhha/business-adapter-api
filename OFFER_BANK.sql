/*
 Navicat Premium Data Transfer

 Source Server         : Business Adapter
 Source Server Type    : MariaDB
 Source Server Version : 100129
 Source Host           : dev.travelcells.com.vn:3306
 Source Schema         : OFFER_BANK

 Target Server Type    : MariaDB
 Target Server Version : 100129
 File Encoding         : 65001

 Date: 01/04/2019 11:02:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for map_service_param
-- ----------------------------
DROP TABLE IF EXISTS `map_service_param`;
CREATE TABLE `map_service_param`  (
  `map_service_param_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `business_param` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bot_param` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `default_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `location` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `create_date` date NOT NULL,
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`map_service_param_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of map_service_param
-- ----------------------------
INSERT INTO `map_service_param` VALUES (4, 1, 'company_business', 'company_bot', 'NHK', 0, 1, '0000-00-00', 'ha', NULL, NULL);
INSERT INTO `map_service_param` VALUES (5, 1, 'body_business', 'body_bot', 'Body', 0, 1, '0000-00-00', 'ha', NULL, NULL);
INSERT INTO `map_service_param` VALUES (6, 1, 'country_business', 'country_bot', 'Country', 1, 1, '0000-00-00', 'ha', NULL, NULL);

-- ----------------------------
-- Table structure for map_template_service
-- ----------------------------
DROP TABLE IF EXISTS `map_template_service`;
CREATE TABLE `map_template_service`  (
  `map_template_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `create_date` date NOT NULL,
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`map_template_service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of map_template_service
-- ----------------------------
INSERT INTO `map_template_service` VALUES (1, 1, 1, 1, 1, '2019-03-27', 'admin', NULL, NULL);
INSERT INTO `map_template_service` VALUES (14, 1, 2, 2, 1, '0000-00-00', 'admin', NULL, '');

-- ----------------------------
-- Table structure for param
-- ----------------------------
DROP TABLE IF EXISTS `param`;
CREATE TABLE `param`  (
  `param_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_param` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `default_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  `create_date` date NOT NULL,
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`param_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for permit_user
-- ----------------------------
DROP TABLE IF EXISTS `permit_user`;
CREATE TABLE `permit_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pass_word` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permit_user
-- ----------------------------
INSERT INTO `permit_user` VALUES (1, 'test', 'test', '1.2.3.4.5');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_code` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `url` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `wsdl` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `template_input` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `template_output` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_return` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` int(20) NOT NULL,
  `user_name_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password_value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_authen` int(255) NOT NULL,
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES (1, 'SOAP_SERVICE', 'http://dev.travelcells.com.vn:8080/ws', '', '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\r\n				  xmlns:gs=\"http://dev.travelcells.com.vn:8080/ws\">\r\n   <soapenv:Header/>\r\n   <soapenv:%body_business%>\r\n      <gs:get%country_business%Request>\r\n         <gs:company>%company_business%</gs:company>\r\n      </gs:get%country_business%Request>\r\n   </soapenv:%body_business%>\r\n</soapenv:Envelope>', '', 0, 1, NULL, NULL, '0000-00-00', 'ha', NULL, NULL, 'POST', 0, NULL, NULL, 0);
INSERT INTO `service` VALUES (2, 'RESTFUL_SERVICE', 'http://dev.travelcells.com.vn:8088/api/business_adapter/runAPI', '', '{\"company\": \"NHK\"}', '', 0, 1, 'USERNAME', 'PASSWORD', '0000-00-00', 'ha', NULL, NULL, 'POST', 1, 'app', '123456', 1);

-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template`  (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `freemaker_output` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int(1) NOT NULL,
  `create_date` date NOT NULL,
  `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`template_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of template
-- ----------------------------
INSERT INTO `template` VALUES (1, 'NHK', 'Xin chào ${input.sender_data.username}!				\r\nBạn đang truy vấn template: ${input.tempale_code}\r\nSố thuê bao của bạn là: ${input.sender_data.mobile}\r\nCác thông tin: 			\r\n<#assign prs = input.params>\r\n<#list 0 ..< prs.length() as i>\r\nTham số: ${prs.get(i).name}\r\nGiá trị: ${prs.get(i).value}\r\n</#list>', 1, '2019-03-27', 'admin', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
