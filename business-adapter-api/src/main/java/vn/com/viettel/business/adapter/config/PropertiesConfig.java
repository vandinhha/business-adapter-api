package vn.com.viettel.business.adapter.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class PropertiesConfig {

	@Value("${api.authen.userName}")
	private String appUserName;
	
	@Value("${api.authen.password}")
	private String appPassword;
	
	@Value("${api.authen.ip}")
	private String ip;
	
	
	@Value("${api.authen.connection.max}")
	private String maxConnection;
	
	@Value("${api.authen.connection.max}")
	private String timeout;
	
	@Value("${code.required}")
	private String codeReq;
	
	@Value("${code.duplicate}")
	private String codeDuplicate;
	
	@Value("${id.required}")
	private String idReq;
	
	@Value("${id.notExits}")
	private String idNotExits;
	
	@Value("${freemakerOutput.required}")
	private String freemakerOutputReq;
	
	@Value("${url.required}")
	private String urlReq;
	
	@Value("${wsdl.required}")
	private String wsdlReq;
	
	@Value("${templateInput.required}")
	private String templateInputReq;
	
	@Value("${templateOutput.required}")
	private String templateOuputReq;
	
	@Value("${isReturn.required}")
	private String isReturnReq;
	
	@Value("${serviceid.required}")
	private String serviceIdReq;
	
	@Value("${business.param.required}")
	private String businessParamReq;
	
	@Value("${bot.param.required}")
	private String botParamReq;
	
	@Value("${location.required}")
	private String locationReq;
	
	@Value("${serviceid.notExits}")
	private String serviceIdNotExits;
	
	@Value("${paramid.notExits}")
	private String paramIdNotExits;
	
	@Value("${templateid.required}")
	private String templateIdReq;
	
	@Value("${templateid.notExits}")
	private String templateIdNotExits;
	
	@Value("${ord.required}")
	private String ordReq;

	@Value("${validate.error}")
	private String validateError;
	
	@Value("${duplicate}")
	private String duplicate;
	
	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getAppPassword() {
		return appPassword;
	}

	public void setAppPassword(String appPassword) {
		this.appPassword = appPassword;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMaxConnection() {
		return maxConnection;
	}

	public void setMaxConnection(String maxConnection) {
		this.maxConnection = maxConnection;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getCodeReq() {
		return codeReq;
	}

	public void setCodeReq(String codeReq) {
		this.codeReq = codeReq;
	}

	public String getCodeDuplicate() {
		return codeDuplicate;
	}

	public void setCodeDuplicate(String codeDuplicate) {
		this.codeDuplicate = codeDuplicate;
	}

	public String getIdReq() {
		return idReq;
	}

	public void setIdReq(String idReq) {
		this.idReq = idReq;
	}

	public String getIdNotExits() {
		return idNotExits;
	}

	public void setIdNotExits(String idNotExits) {
		this.idNotExits = idNotExits;
	}

	public String getFreemakerOutputReq() {
		return freemakerOutputReq;
	}

	public void setFreemakerOutputReq(String freemakerOutputReq) {
		this.freemakerOutputReq = freemakerOutputReq;
	}

	public String getUrlReq() {
		return urlReq;
	}

	public void setUrlReq(String urlReq) {
		this.urlReq = urlReq;
	}

	public String getWsdlReq() {
		return wsdlReq;
	}

	public void setWsdlReq(String wsdlReq) {
		this.wsdlReq = wsdlReq;
	}

	public String getTemplateInputReq() {
		return templateInputReq;
	}

	public void setTemplateInputReq(String templateInputReq) {
		this.templateInputReq = templateInputReq;
	}

	public String getTemplateOuputReq() {
		return templateOuputReq;
	}

	public void setTemplateOuputReq(String templateOuputReq) {
		this.templateOuputReq = templateOuputReq;
	}

	public String getIsReturnReq() {
		return isReturnReq;
	}

	public void setIsReturnReq(String isReturnReq) {
		this.isReturnReq = isReturnReq;
	}

	public String getServiceIdReq() {
		return serviceIdReq;
	}

	public void setServiceIdReq(String serviceIdReq) {
		this.serviceIdReq = serviceIdReq;
	}

	public String getBusinessParamReq() {
		return businessParamReq;
	}

	public void setBusinessParamReq(String businessParamReq) {
		this.businessParamReq = businessParamReq;
	}

	public String getBotParamReq() {
		return botParamReq;
	}

	public void setBotParamReq(String botParamReq) {
		this.botParamReq = botParamReq;
	}

	public String getLocationReq() {
		return locationReq;
	}

	public void setLocationReq(String locationReq) {
		this.locationReq = locationReq;
	}

	public String getServiceIdNotExits() {
		return serviceIdNotExits;
	}

	public void setServiceIdNotExits(String serviceIdNotExits) {
		this.serviceIdNotExits = serviceIdNotExits;
	}

	public String getParamIdNotExits() {
		return paramIdNotExits;
	}

	public void setParamIdNotExits(String paramIdNotExits) {
		this.paramIdNotExits = paramIdNotExits;
	}

	public String getTemplateIdReq() {
		return templateIdReq;
	}

	public void setTemplateIdReq(String templateIdReq) {
		this.templateIdReq = templateIdReq;
	}

	public String getTemplateIdNotExits() {
		return templateIdNotExits;
	}

	public void setTemplateIdNotExits(String templateIdNotExits) {
		this.templateIdNotExits = templateIdNotExits;
	}

	public String getOrdReq() {
		return ordReq;
	}

	public void setOrdReq(String ordReq) {
		this.ordReq = ordReq;
	}

	public String getValidateError() {
		return validateError;
	}

	public void setValidateError(String validateError) {
		this.validateError = validateError;
	}

	public String getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
	
	

}
