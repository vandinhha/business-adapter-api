package vn.com.viettel.business.adapter.model;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.dto.TemplateDTO;

@Entity
@Table(name = "template")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Template implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_sequence")
//    @SequenceGenerator(name = "user_sequence", sequenceName = "PERMIT_USER_SEQ")
	@Id
	@Column(name = "template_id", nullable = false)
	@GeneratedValue(strategy=IDENTITY)
	private Long templateId;

	@Column(name = "template_code", nullable = false)
	private String templateCode;

	@Column(name = "freemaker_output", nullable = false)
	private String freemakerOutput;
		
	@Column(name = "status")
	private Long status;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "create_user")
	private String createUser;
	
	@Column(name = "update_user")
	private String updateUser;
	

	
	
	public Long getTemplateId() {
		return templateId;
	}




	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}




	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}




	public String getFreemakerOutput() {
		return freemakerOutput;
	}




	public void setFreemakerOutput(String freemakerOutput) {
		this.freemakerOutput = freemakerOutput;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public TemplateDTO toDTO() {
		TemplateDTO ret  = new TemplateDTO();
		ret.setTemplateId(this.templateId.toString());
		ret.setTemplateCode(this.templateCode);
		ret.setFreemakerOutput(this.freemakerOutput);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		return ret;
	}
	
	
}
