package vn.com.viettel.business.adapter.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.model.ServiceBO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceDTO {

	private String service_id;
	private String serviceCode;
	private String url;
	private String templateInput;
	private String wsdl;
	private String templateOutput;
	private String userName;
	private String passWord;
	private Long status;
	private String isReturn;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;
	private String order;
	private String method;
	private Long type;
	private String userNameValue;
	private String passWordValue;
	private Long isAuthen;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	
	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTemplateInput() {
		return templateInput;
	}

	public void setTemplateInput(String templateInput) {
		this.templateInput = templateInput;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public String getTemplateOutput() {
		return templateOutput;
	}

	public void setTemplateOutput(String templateOutput) {
		this.templateOutput = templateOutput;
	}

	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	

	public String getIsReturn() {
		return isReturn;
	}

	public void setIsReturn(String isReturn) {
		this.isReturn = isReturn;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUserNameValue() {
		return userNameValue;
	}

	public void setUserNameValue(String userNameValue) {
		this.userNameValue = userNameValue;
	}

	public String getPassWordValue() {
		return passWordValue;
	}

	public void setPassWordValue(String passWordValue) {
		this.passWordValue = passWordValue;
	}

	public Long getIsAuthen() {
		return isAuthen;
	}

	public void setIsAuthen(Long isAuthen) {
		this.isAuthen = isAuthen;
	}

	public ServiceBO toBO() {
		ServiceBO ret = new ServiceBO();
		if(this.service_id!=null) {
		ret.setServiceId(Long.parseLong(this.service_id));
		}
		ret.setServiceCode(this.serviceCode);
		ret.setUrl(this.url);
		ret.setWsdl(this.wsdl);
		ret.setTemplateInput(this.templateInput);
		ret.setTemplateOutput(this.templateOutput);
		ret.setIsReturn(Long.parseLong(this.isReturn));
		ret.setUserName(this.userName);
		ret.setPassWord(this.passWord);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		ret.setMethod(this.method);
		ret.setType(this.type);
		ret.setUserNameValue(userNameValue);
		ret.setPassWordValue(passWordValue);
		ret.setIsAuthen(isAuthen);
		return ret;
	}

}
