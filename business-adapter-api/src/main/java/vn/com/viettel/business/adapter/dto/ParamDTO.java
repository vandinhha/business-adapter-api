package vn.com.viettel.business.adapter.dto;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.model.Param;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ParamDTO implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
	private Long paramId;

	private String businessParam;

	private String defaultValue;
		
	private Long status;
	
	private Date createDate;
	
	private Date updateDate;
	
	private String createUser;
	
	private String updateUser;
	
	public Long getParamId() {
		return paramId;
	}




	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}




	public String getBusinessParam() {
		return businessParam;
	}




	public void setBusinessParam(String businessParam) {
		this.businessParam = businessParam;
	}




	public String getDefaultValue() {
		return defaultValue;
	}




	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public Param toBO() {
		Param ret  = new Param();
		ret.setParamId(this.paramId);
		ret.setBusinessParam(this.businessParam);
		ret.setDefaultValue(this.defaultValue);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateDate(this.updateDate);
		return ret;
	}
	
	
}
