package vn.com.viettel.business.adapter.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.dto.MapServiceParamDTO;
import vn.com.viettel.business.adapter.model.MapServiceParam;
import vn.com.viettel.business.adapter.repositories.MapServiceParamRepository;
import vn.com.viettel.business.adapter.repositories.ParamRepository;
import vn.com.viettel.business.adapter.repositories.ServiceRepository;
import vn.com.viettel.business.adapter.service.MapServiceParamService;
import vn.com.viettel.business.adapter.utils.DataUtil;

@Service
@Transactional
public class MapServiceParamServiceImpl implements MapServiceParamService{
	@Autowired
	MapServiceParamRepository mapServiceParamRepository;
	
	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	ParamRepository paramRepository;
	
	@Autowired
	private PropertiesConfig config;
	
	@Override
	public List<Long> createMapServiceParam(List<MapServiceParamDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getServiceId())){
					throw new Exception(config.getServiceIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getServiceId())){
					throw new Exception(config.getValidateError());
				}
				if(StringUtils.isEmpty(list.get(i).getBusinessParam())){
					throw new Exception(config.getBusinessParamReq());
				}
				if(StringUtils.isEmpty(list.get(i).getBotParam())){
					throw new Exception(config.getBotParamReq());
				}
				if(StringUtils.isEmpty(list.get(i).getLocation())){
					throw new Exception(config.getLocationReq());
				}
				if(!DataUtil.checkLong(list.get(i).getLocation())){
					throw new Exception(config.getValidateError());
				}
				if(serviceRepository.getOne(Long.parseLong(list.get(i).getServiceId()), 1L)==null) {
					throw new Exception(config.getServiceIdNotExits());
				}
				if(paramRepository.getOne(list.get(i).getBusinessParam(), 1L)==null) {
					throw new Exception(config.getParamIdNotExits());
				}
				if(mapServiceParamRepository.findByServiceAndParam(Long.parseLong(list.get(i).getServiceId()),list.get(i).getBusinessParam())!=null) {
					throw new Exception(config.getDuplicate());
				}
				list.get(i).setStatus(1L);
				list.get(i).setCreateDate(new Date());
				list.get(i).setCreateUser(config.getAppUserName());
				Long id =mapServiceParamRepository.save(list.get(i).toBO()).getMapServiceParamId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> updateMapServiceParam(List<MapServiceParamDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getMapServiceParamId())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getMapServiceParamId())) {
					throw new Exception(config.getValidateError());
				}
				MapServiceParam template = mapServiceParamRepository.findByMapServiceParamId(Long.parseLong(list.get(i).getMapServiceParamId()));
				if(template==null) {
					throw new Exception(config.getIdNotExits());
				}
				if(StringUtils.isEmpty(list.get(i).getServiceId())){
					throw new Exception(config.getServiceIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getServiceId())){
					throw new Exception(config.getValidateError());
				}
				if(StringUtils.isEmpty(list.get(i).getBusinessParam())){
					throw new Exception(config.getBusinessParamReq());
				}
				if(StringUtils.isEmpty(list.get(i).getBotParam())){
					throw new Exception(config.getBotParamReq());
				}
				if(StringUtils.isEmpty(list.get(i).getLocation())){
					throw new Exception(config.getLocationReq());
				}
				if(!DataUtil.checkLong(list.get(i).getLocation())){
					throw new Exception(config.getValidateError());
				}
				if(serviceRepository.getOne(Long.parseLong(list.get(i).getServiceId()), 1L)==null) {
					throw new Exception(config.getServiceIdNotExits());
				}
				if(paramRepository.getOne(list.get(i).getBusinessParam(), 1L)==null) {
					throw new Exception(config.getParamIdNotExits());
				}
				if(mapServiceParamRepository.findByServiceAndParam(Long.parseLong(list.get(i).getServiceId()),list.get(i).getBusinessParam())!=null&&Long.parseLong(list.get(i).getMapServiceParamId())!=mapServiceParamRepository.findByServiceAndParam(Long.parseLong(list.get(i).getServiceId()),list.get(i).getBusinessParam()).getMapServiceParamId()) {
					throw new Exception(config.getDuplicate());
				}
				list.get(i).setUpdateDate(new Date());
				list.get(i).setUpdateUser(config.getAppUserName());
				list.get(i).setStatus(template.getStatus());
				list.get(i).setCreateUser(template.getCreateUser());
				list.get(i).setCreateDate(template.getCreateDate());
				Long id =mapServiceParamRepository.save(list.get(i).toBO()).getMapServiceParamId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> deleteMapServiceParam(List<MapServiceParamDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getMapServiceParamId())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getMapServiceParamId())) {
					throw new Exception(config.getValidateError());
				}
				MapServiceParam template = mapServiceParamRepository.findByMapServiceParamId(Long.parseLong(list.get(i).getMapServiceParamId()));
				if(template==null) {
					throw new Exception(config.getIdNotExits());
				}
				template.setStatus(0L);
				mapServiceParamRepository.save(template);
				listId.add(Long.parseLong(list.get(i).getMapServiceParamId()));
			}
		}
		return listId;
	}

}
