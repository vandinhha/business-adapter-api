package vn.com.viettel.business.adapter.resource;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.constant.AppConst;
import vn.com.viettel.business.adapter.constant.AppConst.LogAction;
import vn.com.viettel.business.adapter.dto.BusinessAdapterDTO;
import vn.com.viettel.business.adapter.model.MapTemplateService;
import vn.com.viettel.business.adapter.model.ServiceBO;
import vn.com.viettel.business.adapter.model.Template;
import vn.com.viettel.business.adapter.repositories.MapTemplateServiceRepository;
import vn.com.viettel.business.adapter.repositories.ServiceRepository;
import vn.com.viettel.business.adapter.repositories.TemplateRepository;
import vn.com.viettel.business.adapter.resource.response.BaseResponse;
import vn.com.viettel.business.adapter.service.BusinessAdapterService;
import vn.com.viettel.business.adapter.service.TemplateService;
import vn.com.viettel.business.adapter.utils.CacheUtils;
import vn.com.viettel.business.adapter.utils.LogCustom;
import vn.com.viettel.business.adapter.utils.LogInfo;
import vn.com.viettel.business.adapter.utils.ResponseUtils;
import vn.com.viettel.business.adapter.utils.StringUtils;
import vn.com.viettel.business.adapter.utils.WebserviceUtils;
import org.apache.log4j.Logger;

@RestController
@RequestMapping("/api/business_adapter")
public class BusinessAdapterResource {

	private static final Logger LOGGER = Logger.getLogger(BusinessAdapterResource.class);
	private static WebserviceUtils webserviceUtils;
	
	@Autowired
	private ApplicationContext context;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	BusinessAdapterService businessAdapterService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private TemplateRepository templateRepository;

	@Autowired
	private MapTemplateServiceRepository mapTemplateServiceRepository;

	@Autowired
	ServiceRepository serviceRepository;

	@Autowired
	private static PropertiesConfig config;

	// manage number of request
	static {
		Long maxConnection = 0L;
		try {
			maxConnection = Long.valueOf(config.getMaxConnection());// 300
		} catch (Exception e) {
			LOGGER.error("Exception: ", e);
			maxConnection = 500L;
		}
		webserviceUtils = new WebserviceUtils(maxConnection, LOGGER);
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@PostMapping("/runService")
	@LogInfo(action = LogAction.READ, comment = "runService")
	public ResponseEntity<BaseResponse<Object>> runService(Locale locale, @RequestBody BusinessAdapterDTO input,
			HttpServletRequest request) {
		try {

			LOGGER.info("-------------------------");
			LOGGER.info("runService API is called");
			LOGGER.info("-------------------------");
			
			// Check max connection
			boolean isReturn = false;
			isReturn = webserviceUtils.prepareConnection();
			
			if (isReturn) {
				LOGGER.error("max connection");
				return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_CONNECTION_MAX,
						messageSource.getMessage("app.connection.max", null, locale), null));
			}
			Date startTime = new Date();
			String transactionId = "";
			transactionId = StringUtils.getTransactionId();
		
			// Star Business
			LogCustom.writeLog(transactionId + ":" + AppConst.SYSTEM_NAME, "start_receiveQuestion", "receiveQuestion", "BusinessAdapterResource", null, null, null, null, startTime);

			JSONObject inputJson = new JSONObject(input);
			Map output = new HashMap<>();
			output.put("input", inputJson);
			
			//Clear Cache => comment this block when deploy
			CacheManager cacheManager = (CacheManager) context.getBean(CacheManager.class);
			CacheUtils.clearCache(cacheManager);

			// Lấy template
			List<Template> listTemp = new ArrayList<>();
			List<Template> temps = templateRepository.findByTemplateCode(input.getTempale_code(), 1L);
			List<Template> tempsss = templateService.findByTemplateCode(input.getTempale_code(), 1L);
			if (temps != null && temps.size() > 0) {
				listTemp = temps;
			}

			// Lấy các MapTemplateService tương ứng
			List<MapTemplateService> listMapTemplateService = new ArrayList<>();

			if (listTemp.size() > 0) {
				List<MapTemplateService> mapTemplateService = mapTemplateServiceRepository
						.findAllByTemplateId(temps.get(0).getTemplateId(), 1L);
				if (mapTemplateService != null && mapTemplateService.size() > 0) {
					listMapTemplateService = mapTemplateService;
				}
			}

			// Lấy list service tương ứng
			List<ServiceBO> listService = new ArrayList();
			for (MapTemplateService mapTemplateService : listMapTemplateService) {
				ServiceBO item = serviceRepository.getOne(mapTemplateService.getServiceId(), 1L);
				if (item != null) {
					listService.add(item);
				}
			}

			// Duyệt các service theo order, gọi service, lấy kết quả trả về đẩy vào hashmap
			for (ServiceBO service : listService) {
				if (service.getType() == 0) {
					JSONObject jsonResponse = businessAdapterService.getSoapResponse(service, input);
					output.put(service.getServiceCode(), jsonResponse);
				} else {
					// Gọi rest api ở đây
					JSONObject jsonResponse = businessAdapterService.getRestResponse(service, input);
					output.put(service.getServiceCode(), jsonResponse);
				}
			}

			// Gọi và fill vào template, trả về cho bot core json string
			Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			cfg.setLogTemplateExceptions(false);
			cfg.setWrapUncheckedExceptions(true);
			StringTemplateLoader stringLoader = new StringTemplateLoader();

			String freemaker_output = "";
			if (listTemp.size() > 0 && listTemp.get(0).getFreemakerOutput() != null) {
				freemaker_output = listTemp.get(0).getFreemakerOutput();
			}
			stringLoader.putTemplate("template", freemaker_output);
			cfg.setTemplateLoader(stringLoader);
			freemarker.template.Template temp = cfg.getTemplate("template");
			Writer out = new StringWriter();
			temp.process(output, out);
			String val = out.toString();
			System.out.println(val);

			JSONObject jsonOutput = new JSONObject(output);
			
			// End business
			long endTime = System.currentTimeMillis();
            LOGGER.info("transactionId: " + transactionId + ", duration handleQuestion: " + (endTime - startTime.getTime()) / 1000);

			return ResponseEntity.ok()
					.body(ResponseUtils.createResponse(BaseResponse.CODE_SUCCESS, "Success", jsonOutput.toString()));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR,
					messageSource.getMessage("app.internal.error", null, locale), null));
		} finally {
			webserviceUtils.releaseRequest();
		}

	}
	
	@PostMapping("/clearCache")
	@LogInfo(action = LogAction.READ, comment = "clearCache")
	public ResponseEntity<BaseResponse<Object>> clearCache(Locale locale, HttpServletRequest request) {
		CacheManager cacheManager = (CacheManager) context.getBean(CacheManager.class);
		CacheUtils.clearCache(cacheManager);
	return null;
	}

}
