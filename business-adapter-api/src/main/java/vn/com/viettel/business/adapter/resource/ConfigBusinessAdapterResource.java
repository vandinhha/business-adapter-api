package vn.com.viettel.business.adapter.resource;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.constant.AppConst.LogAction;
import vn.com.viettel.business.adapter.dto.ConfigBusinessAdapterDTO;
import vn.com.viettel.business.adapter.dto.MapServiceParamDTO;
import vn.com.viettel.business.adapter.dto.MapTemplateServiceDTO;
import vn.com.viettel.business.adapter.dto.ServiceDTO;
import vn.com.viettel.business.adapter.dto.TemplateDTO;
import vn.com.viettel.business.adapter.model.Template;
import vn.com.viettel.business.adapter.resource.response.BaseResponse;
import vn.com.viettel.business.adapter.service.BusinessAdapterService;
import vn.com.viettel.business.adapter.service.MapServiceParamService;
import vn.com.viettel.business.adapter.service.MapTemplateServiceService;
import vn.com.viettel.business.adapter.service.ServiceService;
import vn.com.viettel.business.adapter.service.TemplateService;
import vn.com.viettel.business.adapter.utils.LogInfo;
import vn.com.viettel.business.adapter.utils.ResponseUtils;
@RestController
@RequestMapping("/api/business_adapter")
public class ConfigBusinessAdapterResource {
	Logger log = LoggerFactory.getLogger(BusinessAdapterResource.class);

	public static final String PREFIX_ISDN = "isdn_";

	@Autowired
	MessageSource messageSource;

	@Autowired
	BusinessAdapterService businessAdapterService;

	@Autowired
	private TemplateService templateService;
	
	@Autowired
	private ServiceService serviceService;

	@Autowired
	private MapTemplateServiceService mapTemplateServiceService;
	
	@Autowired
	private MapServiceParamService mapServiceParamService;

	@Autowired
	private PropertiesConfig config;
	
	@PostMapping("/configBusinessAdapterApi")
	@LogInfo(action = LogAction.READ, comment = "configBusinessAdapterApi")
	public ResponseEntity<BaseResponse<Object>> configBusinessAdapterApi(Locale locale, @RequestBody ConfigBusinessAdapterDTO input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<ServiceDTO> lstService = input.getServices();
			List<Template> temps = templateService.findByTemplateCode(input.getTempale_code(), 1L);	
			if(lstService!=null&&lstService.size()>0&&temps!=null&&temps.size()>0) {
				for(ServiceDTO serviceDto:lstService) {
					MapTemplateServiceDTO mapTemplateServiceDTO = new MapTemplateServiceDTO();
					mapTemplateServiceDTO.setServiceId(serviceDto.getService_id().toString());
					mapTemplateServiceDTO.setTemplateId(temps.get(0).getTemplateId().toString());
					mapTemplateServiceDTO.setOrder(serviceDto.getOrder());
					mapTemplateServiceDTO.setStatus(1L);
					mapTemplateServiceDTO.setCreateDate(new Date());
					mapTemplateServiceDTO.setCreateUser(config.getAppUserName());
					mapTemplateServiceService.save(mapTemplateServiceDTO.toBO());
				}
			}

			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SUCCESS, "Success", input.getTempale_code()));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_SYSTEM_ERR,
					messageSource.getMessage("app.internal.error", null, locale), null));
		}

	}
	
	@PostMapping("/createTemplate")
	@LogInfo(action = LogAction.READ, comment = "createTemplate")
	public ResponseEntity<BaseResponse<List<Long>>> createTemplate(Locale locale, @RequestBody List<TemplateDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=templateService.createTemplate(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/updateTemplate")
	@LogInfo(action = LogAction.READ, comment = "updateTemplate")
	public ResponseEntity<BaseResponse<List<Long>>> updateTemplate(Locale locale, @RequestBody List<TemplateDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=templateService.updateTemplate(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/deleteTemplate")
	@LogInfo(action = LogAction.READ, comment = "deleteTemplate")
	public ResponseEntity<BaseResponse<List<Long>>> deleteTemplate(Locale locale, @RequestBody List<TemplateDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=templateService.deleteTemplate(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/createService")
	@LogInfo(action = LogAction.READ, comment = "createService")
	public ResponseEntity<BaseResponse<List<Long>>> createService(Locale locale, @RequestBody List<ServiceDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=serviceService.createService(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/updateService")
	@LogInfo(action = LogAction.READ, comment = "updateService")
	public ResponseEntity<BaseResponse<List<Long>>> updateService(Locale locale, @RequestBody List<ServiceDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=serviceService.updateService(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/deleteService")
	@LogInfo(action = LogAction.READ, comment = "deleteService")
	public ResponseEntity<BaseResponse<List<Long>>> deleteService(Locale locale, @RequestBody List<ServiceDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=serviceService.deleteService(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	@PostMapping("/createMapServiceParam")
	@LogInfo(action = LogAction.READ, comment = "createMapServiceParam")
	public ResponseEntity<BaseResponse<List<Long>>> createMapServiceParam(Locale locale, @RequestBody List<MapServiceParamDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=mapServiceParamService.createMapServiceParam(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/updateMapServiceParam")
	@LogInfo(action = LogAction.READ, comment = "updateMapServiceParam")
	public ResponseEntity<BaseResponse<List<Long>>> updateMapServiceParam(Locale locale, @RequestBody List<MapServiceParamDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=mapServiceParamService.updateMapServiceParam(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/deleteMapServiceParam")
	@LogInfo(action = LogAction.READ, comment = "deleteMapServiceParam")
	public ResponseEntity<BaseResponse<List<Long>>> deleteMapServiceParam(Locale locale, @RequestBody List<MapServiceParamDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=mapServiceParamService.deleteMapServiceParam(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	
	@PostMapping("/deleteMapTemplateService")
	@LogInfo(action = LogAction.READ, comment = "deleteMapTemplateService")
	public ResponseEntity<BaseResponse<List<Long>>> deleteMapTemplateService(Locale locale, @RequestBody List<MapTemplateServiceDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=mapTemplateServiceService.deleteMapTemplateService(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
	@PostMapping("/createMapTemplateService")
	@LogInfo(action = LogAction.READ, comment = "createMapTemplateService")
	public ResponseEntity<BaseResponse<List<Long>>> createMapTemplateService(Locale locale, @RequestBody List<ConfigBusinessAdapterDTO> input, HttpServletRequest  request){

		try {
			System.out.println("Đã gọi vào hàm");
			List<Long> listId=mapTemplateServiceService.createMapTemplateService(input);
			return ResponseEntity.ok().body(ResponseUtils.createResponseListId(BaseResponse.CODE_SUCCESS, "Success",listId));
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseUtils.createResponse(BaseResponse.CODE_ERROR,e.getMessage(), null));
		}

	}
}
