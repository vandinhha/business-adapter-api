package vn.com.viettel.business.adapter.dto;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.model.Template;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateDTO{

	
	private String templateId;
	private String templateCode;
	private String freemakerOutput;
	private Long status;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;
	
	


	public String getTemplateId() {
		return templateId;
	}




	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}




	public String getTemplateCode() {
		return templateCode;
	}




	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}




	public String getFreemakerOutput() {
		return freemakerOutput;
	}




	public void setFreemakerOutput(String freemakerOutput) {
		this.freemakerOutput = freemakerOutput;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public Template toBO() {
		Template ret  = new Template();
		if(this.templateId!=null) {
		ret.setTemplateId(Long.parseLong(this.templateId));
		}
		ret.setTemplateCode(this.templateCode);
		ret.setFreemakerOutput(this.freemakerOutput);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		return ret;
	}
	
	
}
