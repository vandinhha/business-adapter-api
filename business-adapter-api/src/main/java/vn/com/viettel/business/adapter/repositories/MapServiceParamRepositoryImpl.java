package vn.com.viettel.business.adapter.repositories;


import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.model.MapServiceParam;
import vn.com.viettel.business.adapter.model.Template;



@Repository
@Transactional
public class MapServiceParamRepositoryImpl implements MapServiceParamRepository {
	@PersistenceContext
    EntityManager entityManager;
	@Override
	public List<MapServiceParam> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MapServiceParam> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MapServiceParam> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapServiceParam> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends MapServiceParam> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<MapServiceParam> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MapServiceParam getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapServiceParam> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapServiceParam> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<MapServiceParam> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MapServiceParam save(MapServiceParam entity) {
		if (entity.getMapServiceParamId() == null) {
		      entityManager.persist(entity);
		      return entity;
		    } else {
		      return entityManager.merge(entity);
		    }
	}

	@Override
	public Optional<MapServiceParam> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(MapServiceParam entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends MapServiceParam> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends MapServiceParam> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapServiceParam> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapServiceParam> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends MapServiceParam> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	//@Cacheable(value = "findAllMapServiceParamByServiceIdAndLocation")
	public List<MapServiceParam> findAllByServiceIdAndLocation(Long serviceId, Long location, Long status) {
		Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_service_param as tm " +
                "WHERE tm.service_id =:serviceId AND tm.location=:location AND tm.status=:status", MapServiceParam.class);
        query.setParameter("serviceId", serviceId);
        query.setParameter("location", location);
        query.setParameter("status", status);
        return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public MapServiceParam findByMapServiceParamId(Long id) {
		 Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_service_param as tm " +
	                "WHERE tm.map_service_param_id =:id AND status=:status", MapServiceParam.class);
	        query.setParameter("id",id);
	        query.setParameter("status",1L);
	        List<MapServiceParam> lst= query.getResultList();
	        if(lst.size()>0){
	        	return lst.get(0);
	        }
	        return null;
	}

	@Override
	public MapServiceParam findByServiceAndParam(Long serviceId, String paramCode) {
		 Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_service_param as tm " +
	                "WHERE tm.service_id =:serviceId AND business_param=:paramCode AND status=:status", MapServiceParam.class);
	        query.setParameter("serviceId",serviceId);
	        query.setParameter("paramCode",paramCode);
	        query.setParameter("status",1L);
	        List<MapServiceParam> lst= query.getResultList();
	        if(lst.size()>0){
	        	return lst.get(0);
	        }
	        return null;
	}

	
	
}
