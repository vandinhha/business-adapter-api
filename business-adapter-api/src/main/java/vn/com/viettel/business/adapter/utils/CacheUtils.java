package vn.com.viettel.business.adapter.utils;


import org.springframework.cache.CacheManager;

public class CacheUtils {
	public static void clearCache(CacheManager cacheManager) {
		if (cacheManager !=null && cacheManager.getCacheNames() != null) {
			for (String name : cacheManager.getCacheNames()) {
				cacheManager.getCache(name).clear();
			}
		}
	}

}
