package vn.com.viettel.business.adapter.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.dto.ConfigBusinessAdapterDTO;
import vn.com.viettel.business.adapter.dto.MapTemplateServiceDTO;
import vn.com.viettel.business.adapter.dto.ServiceDTO;
import vn.com.viettel.business.adapter.model.MapTemplateService;
import vn.com.viettel.business.adapter.repositories.MapTemplateServiceRepositoryImpl;
import vn.com.viettel.business.adapter.repositories.ServiceRepository;
import vn.com.viettel.business.adapter.repositories.TemplateRepositoryImpl;
import vn.com.viettel.business.adapter.service.MapTemplateServiceService;
import vn.com.viettel.business.adapter.utils.DataUtil;

@Service
@Transactional
public class MapTemplateServiceServiceImpl implements MapTemplateServiceService {

	@Autowired
	MapTemplateServiceRepositoryImpl mapTemplateServiceRepositoryImpl;
	
	@Autowired
	TemplateRepositoryImpl templateRepositoryImpl;
	
	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	private PropertiesConfig config;

	@Override
	public MapTemplateService save(MapTemplateService entity) {
		// TODO Auto-generated method stub
		return mapTemplateServiceRepositoryImpl.save(entity);
	}

	@Override
	public List<Long> deleteMapTemplateService(List<MapTemplateServiceDTO> list) throws Exception{
		List<Long> listId = new ArrayList<Long>();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if(StringUtils.isEmpty(list.get(i).getMapTemplateServiceId())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getMapTemplateServiceId())) {
					throw new Exception(config.getValidateError());
				}
				MapTemplateService template = mapTemplateServiceRepositoryImpl
						.findByMapTemplateServiceId(Long.parseLong(list.get(i).getMapTemplateServiceId()));
				if (template == null) {
					throw new Exception(config.getIdNotExits());
				}
					template.setStatus(0L);
					mapTemplateServiceRepositoryImpl.save(template);
					listId.add(Long.parseLong(list.get(i).getMapTemplateServiceId()));
			}
		}
		return listId;
	}

	@Override
	public List<Long> createMapTemplateService(List<ConfigBusinessAdapterDTO> list) throws Exception{
		List<Long> listId = new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getTemplateId())) {
					throw new Exception(config.getTemplateIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getTemplateId())) {
					throw new Exception(config.getValidateError());
				}
				if(templateRepositoryImpl.findByTemplateId(Long.parseLong(list.get(i).getTemplateId()))==null) {
					throw new Exception(config.getTemplateIdNotExits());
				}
		List<ServiceDTO> lstService = list.get(i).getServices();
		if(lstService!=null&&lstService.size()>0) {
			for(ServiceDTO serviceDto:lstService) {
				if(StringUtils.isEmpty(serviceDto.getService_id())) {
					throw new Exception(config.getServiceIdReq());
				}
				if(!DataUtil.checkLong(serviceDto.getService_id())) {
					throw new Exception(config.getValidateError());
				}
				if(StringUtils.isEmpty(serviceDto.getOrder())) {
					throw new Exception(config.getOrdReq());
				}
				if(!DataUtil.checkLong(serviceDto.getOrder())) {
					throw new Exception(config.getValidateError());
				}
				if(serviceRepository.getOne(Long.parseLong(serviceDto.getService_id()),1L)==null) {
					throw new Exception(config.getServiceIdNotExits());
				}
				if(mapTemplateServiceRepositoryImpl.findByTemplateAndService(Long.parseLong(serviceDto.getService_id()),Long.parseLong(list.get(i).getTemplateId()))!=null) {
					throw new Exception(config.getDuplicate());
				}
				MapTemplateServiceDTO mapTemplateServiceDTO = new MapTemplateServiceDTO();
				mapTemplateServiceDTO.setServiceId(serviceDto.getService_id());
				mapTemplateServiceDTO.setTemplateId(list.get(i).getTemplateId());
				mapTemplateServiceDTO.setOrder(serviceDto.getOrder());
				mapTemplateServiceDTO.setStatus(1L);
				mapTemplateServiceDTO.setCreateDate(new Date());
				mapTemplateServiceDTO.setCreateUser(config.getAppUserName());
				MapTemplateService mapObj=mapTemplateServiceRepositoryImpl.save(mapTemplateServiceDTO.toBO());
				listId.add(mapObj.getMapTemplateServiceId());
					}
				}
			}
			
	}
		return listId;
	}
}
