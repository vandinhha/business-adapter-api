package vn.com.viettel.business.adapter.model;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.dto.MapServiceParamDTO;

@Entity
@Table(name = "map_service_param")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MapServiceParam implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
	@Id
	@Column(name = "map_service_param_id", nullable = false)
	@GeneratedValue(strategy=IDENTITY)
	private Long mapServiceParamId;

	@Column(name = "service_id", nullable = false)
	private Long serviceId;

	@Column(name = "business_param", nullable = false)
	private String businessParam;
	
	@Column(name = "default_value")
	private String defaultValue;
	
	@Column(name = "location")
	private Long location;
	
	@Column(name = "bot_param")
	private String botParam;
		
	@Column(name = "status")
	private Long status;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "create_user")
	private String createUser;
	
	@Column(name = "update_user")
	private String updateUser;
	


	public Long getMapServiceParamId() {
		return mapServiceParamId;
	}




	public void setMapServiceParamId(Long mapServiceParamId) {
		this.mapServiceParamId = mapServiceParamId;
	}




	public Long getServiceId() {
		return serviceId;
	}




	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}




	public String getBusinessParam() {
		return businessParam;
	}




	public void setBusinessParam(String businessParam) {
		this.businessParam = businessParam;
	}




	public String getDefaultValue() {
		return defaultValue;
	}




	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}




	public Long getLocation() {
		return location;
	}




	public void setLocation(Long location) {
		this.location = location;
	}




	public String getBotParam() {
		return botParam;
	}




	public void setBotParam(String botParam) {
		this.botParam = botParam;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public MapServiceParamDTO toDTO() {
		MapServiceParamDTO ret  = new MapServiceParamDTO();
		ret.setMapServiceParamId(this.mapServiceParamId.toString());
		ret.setServiceId(this.serviceId.toString());
		ret.setBusinessParam(this.businessParam);
		ret.setBotParam(this.botParam);
		ret.setDefaultValue(this.defaultValue);
		ret.setLocation(this.location.toString());
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		return ret;
	}
	
	
}
