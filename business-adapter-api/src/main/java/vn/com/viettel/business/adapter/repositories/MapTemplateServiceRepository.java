package vn.com.viettel.business.adapter.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.business.adapter.model.MapTemplateService;;

public interface MapTemplateServiceRepository extends JpaRepository<MapTemplateService, Long> {
	
	List<MapTemplateService> findAllByTemplateId(Long templateId, Long status);
	
	public MapTemplateService findByMapTemplateServiceId(Long id);
	public MapTemplateService findByTemplateAndService(Long templateId,Long serviceId);
}
