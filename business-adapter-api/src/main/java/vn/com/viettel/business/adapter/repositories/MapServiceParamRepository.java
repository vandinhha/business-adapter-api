package vn.com.viettel.business.adapter.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.business.adapter.model.MapServiceParam;;

public interface MapServiceParamRepository extends JpaRepository<MapServiceParam, Long> {
	
	List<MapServiceParam> findAllByServiceIdAndLocation(Long serviceId, Long location, Long status);
	public MapServiceParam findByMapServiceParamId(Long id);
	public MapServiceParam findByServiceAndParam(Long serviceId,String paramCode);

}
