package vn.com.viettel.business.adapter.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.dto.TemplateDTO;
import vn.com.viettel.business.adapter.model.Template;
import vn.com.viettel.business.adapter.repositories.TemplateRepository;
import vn.com.viettel.business.adapter.service.TemplateService;
import vn.com.viettel.business.adapter.utils.DataUtil;

@Service
@Transactional
public class TemplateServiceImpl implements TemplateService{

	@Autowired
	TemplateRepository templateRepository;
	
	@Autowired
	private PropertiesConfig config;
	
	public List<Template> findByTemplateCode(String templateCode, Long status){
		return templateRepository.findByTemplateCode(templateCode, status);
	}

	@Override
	public List<Long> createTemplate(List<TemplateDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getTemplateCode())) {
					throw new Exception(config.getCodeReq());
				}
				if(StringUtils.isEmpty(list.get(i).getFreemakerOutput())) {
					throw new Exception(config.getFreemakerOutputReq());
				}
				if(findByTemplateCode(list.get(i).getTemplateCode(), 1L)!=null&&findByTemplateCode(list.get(i).getTemplateCode(), 1L).size()>0) {
					throw new Exception(config.getCodeDuplicate());
				}
				list.get(i).setStatus(1L);
				list.get(i).setCreateDate(new Date());
				list.get(i).setCreateUser(config.getAppUserName());
				Long id =templateRepository.save(list.get(i).toBO()).getTemplateId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> updateTemplate(List<TemplateDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getTemplateId())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getTemplateId())) {
					throw new Exception(config.getValidateError());
				}
				Template template = templateRepository.findByTemplateId(Long.parseLong(list.get(i).getTemplateId()));
				if(template==null) {
					throw new Exception(config.getIdNotExits());
				}
				if(StringUtils.isEmpty(list.get(i).getTemplateCode())) {
					throw new Exception(config.getCodeReq());
				}
				if(StringUtils.isEmpty(list.get(i).getFreemakerOutput())) {
					throw new Exception(config.getFreemakerOutputReq());
				}
				if(findByTemplateCode(list.get(i).getTemplateCode(), 1L)!=null&&findByTemplateCode(list.get(i).getTemplateCode(), 1L).size()>0&&Long.parseLong(list.get(i).getTemplateId())!=findByTemplateCode(list.get(i).getTemplateCode(), 1L).get(0).getTemplateId()) {
					throw new Exception(config.getCodeDuplicate());
				}
				list.get(i).setUpdateDate(new Date());
				list.get(i).setStatus(template.getStatus());
				list.get(i).setUpdateUser(config.getAppUserName());
				list.get(i).setCreateUser(template.getCreateUser());
				list.get(i).setCreateDate(template.getCreateDate());
				Long id =templateRepository.save(list.get(i).toBO()).getTemplateId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> deleteTemplate(List<TemplateDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getTemplateId())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getTemplateId())) {
					throw new Exception(config.getValidateError());
				}
				Template template = templateRepository.findByTemplateId(Long.parseLong(list.get(i).getTemplateId()));
				if(template==null) {
					throw new Exception(config.getIdNotExits());
				}
				template.setStatus(0L);
				templateRepository.save(template);
				listId.add(Long.parseLong(list.get(i).getTemplateId()));
			}
		}
		return listId;
	}
	
}
