package vn.com.viettel.business.adapter.utils;

public final class CommonUtils {
	
	public static boolean isNullOrEmpty(String value) {
		if (value == null) {
			return true;
		}
		if (value.isEmpty()) {
			return true;
		}
		return false;
	}
}
