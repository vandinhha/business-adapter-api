package vn.com.viettel.business.adapter.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigBusinessAdapterDTO {
	private String tempale_code;
	private String templateId;
	private List<ServiceDTO> services;
	public String getTempale_code() {
		return tempale_code;
	}
	public void setTempale_code(String tempale_code) {
		this.tempale_code = tempale_code;
	}
	public List<ServiceDTO> getServices() {
		return services;
	}
	public void setServices(List<ServiceDTO> services) {
		this.services = services;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	
	
	
}
