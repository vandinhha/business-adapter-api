package vn.com.viettel.business.adapter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.cloud.aws.autoconfigure.cache.ElastiCacheAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import vn.com.viettel.business.adapter.config.LogRequestFilter;
import vn.com.viettel.business.adapter.config.PropertiesConfig;

@Configuration
@ComponentScan
@EnableAutoConfiguration(exclude = ElastiCacheAutoConfiguration.class)
@EnableConfigurationProperties({PropertiesConfig.class})
@SpringBootApplication
@EnableCaching
public class BusinessAdapterApiApplication {
	
	static Logger log = LoggerFactory.getLogger(BusinessAdapterApiApplication.class);

	public static void main(String[] args) throws UnknownHostException {
		
		SpringApplication app = new SpringApplication(BusinessAdapterApiApplication.class);
        Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
            env.getActiveProfiles());
	}
	
    @Bean
    public FilterRegistrationBean loggingFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LogRequestFilter());
// In case you want the filter to apply to specific URL patterns only
        registration.addUrlPatterns("/api/*");
        return registration;
    }
    
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        List<Cache> caches = new ArrayList<Cache>();
        caches.add(new ConcurrentMapCache("findTemplateByTemplateCode"));
        caches.add(new ConcurrentMapCache("getOneServiceByServiceId"));
        caches.add(new ConcurrentMapCache("findAllMapTemplateServiceByTemplateId"));
        caches.add(new ConcurrentMapCache("findAllMapServiceParamByServiceIdAndLocation"));
        cacheManager.setCaches(caches);
        return cacheManager;
    }
}
