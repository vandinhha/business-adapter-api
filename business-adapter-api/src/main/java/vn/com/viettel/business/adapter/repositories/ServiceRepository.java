package vn.com.viettel.business.adapter.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.business.adapter.model.ServiceBO;;

public interface ServiceRepository extends JpaRepository<ServiceBO, Long> {

	ServiceBO getOne(Long id, Long status);	
	List<ServiceBO> findByServiceCode(String templateCode, Long status);

}
