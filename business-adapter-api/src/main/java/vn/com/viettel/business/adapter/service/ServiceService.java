package vn.com.viettel.business.adapter.service;

import java.util.List;

import vn.com.viettel.business.adapter.dto.ServiceDTO;

public interface ServiceService {
	List<Long> createService(List<ServiceDTO> list) throws Exception;
	List<Long> updateService(List<ServiceDTO> list) throws Exception;
	List<Long> deleteService(List<ServiceDTO> list) throws Exception;
}
