package vn.com.viettel.business.adapter.service;

import java.util.List;

import vn.com.viettel.business.adapter.dto.TemplateDTO;
import vn.com.viettel.business.adapter.model.Template;

public interface TemplateService {
	List<Template> findByTemplateCode(String templateCode, Long status);
	List<Long> createTemplate(List<TemplateDTO> list) throws Exception;
	List<Long> updateTemplate(List<TemplateDTO> list) throws Exception;
	List<Long> deleteTemplate(List<TemplateDTO> list) throws Exception;
}
