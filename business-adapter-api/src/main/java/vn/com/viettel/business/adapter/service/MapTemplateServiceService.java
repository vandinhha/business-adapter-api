package vn.com.viettel.business.adapter.service;

import java.util.List;

import vn.com.viettel.business.adapter.dto.ConfigBusinessAdapterDTO;
import vn.com.viettel.business.adapter.dto.MapTemplateServiceDTO;
import vn.com.viettel.business.adapter.model.MapTemplateService;

public interface MapTemplateServiceService {
	public  MapTemplateService  save(MapTemplateService entity);
	List<Long> deleteMapTemplateService(List<MapTemplateServiceDTO> list) throws Exception;
	List<Long> createMapTemplateService(List<ConfigBusinessAdapterDTO> list) throws Exception;
}	
