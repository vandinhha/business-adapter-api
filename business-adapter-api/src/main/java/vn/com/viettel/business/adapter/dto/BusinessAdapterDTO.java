package vn.com.viettel.business.adapter.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessAdapterDTO {
	private String tempale_code;
	private SenderDataDTO sender_data;
	private List<BotParamDTO> params;
//	private List<ServiceDTO> services;
	public String getTempale_code() {
		return tempale_code;
	}
	public void setTempale_code(String tempale_code) {
		this.tempale_code = tempale_code;
	}
	
	public SenderDataDTO getSender_data() {
		return sender_data;
	}
	public void setSender_data(SenderDataDTO sender_data) {
		this.sender_data = sender_data;
	}
	public List<BotParamDTO> getParams() {
		return params;
	}
	public void setParams(List<BotParamDTO> params) {
		this.params = params;
	}
	
//	public List<ServiceDTO> getServices() {
//		return services;
//	}
//	public void setServices(List<ServiceDTO> services) {
//		this.services = services;
//	}
	
}
