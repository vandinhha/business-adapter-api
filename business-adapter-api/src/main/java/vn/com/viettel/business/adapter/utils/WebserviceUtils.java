package vn.com.viettel.business.adapter.utils;

import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Logger;

public class WebserviceUtils {

    private Logger logger;
    private AtomicInteger totalCurrentRequest = new AtomicInteger(0);
    private Long maxConnection;//= Long.valueOf(PropertiesUtils.getResource("maxConnection"));
    private String wsCode;

    public WebserviceUtils(Long maxConnection, Logger logger) {
        this.maxConnection = maxConnection;
        this.logger = logger;
    }

    public boolean prepareConnection() {
        boolean isReturn = false;
        try {
            increase();
            if (totalCurrentRequest.get() > maxConnection.intValue()) {
                logger.info("wsCode: " + wsCode + ",totalCurrentRequest: " + totalCurrentRequest + ", maxConnection : " + maxConnection);
                isReturn = true;
            } else if (totalCurrentRequest.get() > 100) {
                logger.info("wsCode: " + wsCode + ",totalCurrentRequest: " + totalCurrentRequest);
            }
        } catch (Exception e) {
            logger.error("Have error", e);
        }
        return isReturn;
    }

    /**
     * Tang request len 1
     */
    public /*synchronized*/ void increase() {
        totalCurrentRequest.incrementAndGet();
    }

    /**
     * Giai phong request
     */
    public /*synchronized*/ void releaseRequest() {
        if (totalCurrentRequest.get() > 0) {
            totalCurrentRequest.decrementAndGet();
        }
    }

    public /*synchronized*/ int getTotalCurrentRequest() {
        return totalCurrentRequest.get();
    }

    public boolean isMaxConnection() {
        return totalCurrentRequest.get() >= maxConnection.intValue();
    }

}