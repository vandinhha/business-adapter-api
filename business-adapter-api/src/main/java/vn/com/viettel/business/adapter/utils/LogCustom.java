package vn.com.viettel.business.adapter.utils;

import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.log4j.Logger;

/**
 *
 * @author Toancx
 */
public class LogCustom {

    private static final Logger logger = Logger.getLogger("loggerAction");
    private static final Logger loggerKpi = Logger.getLogger("loggerActionKpi");
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";

    private LogCustom() {
    }

    /**
     * @param logType : start_action, end_action, start_connect, end_connect
     * @param systemName : Ten he thong
     * @param threadName : Ten thread
     * @param className : Ten class thuc hien thread
     * @param uri : url he thong ket noi den
     * @param numOfProcessingRecord : So ban ghi xu ly
     * @param numOfSuccessRecord : So ban ghi xu ly thanh cong
     * @param status : LogAction.SUCCESS, LogAction.FAIL
     * @param startTime : Thoi gian bat dau tien trinh hoac bat dau ket noi
     * @param isOke
     */
    public static void writeLog(String systemName, String logType, String threadName, String className, String uri, String numOfProcessingRecord, String numOfSuccessRecord,
                                String status, Date startTime, boolean isOke) {
        InetAddress ipAddress = null;
        String ip = "";
        Date endTime;
        String userName = "Application";
        String path = "";
        String port ;
        String param = "";
        String clientKpiId = "";
        Long duration ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS", Locale.ENGLISH);

        String[] params = systemName.split(":");
        String callId = params[0];
        systemName = params[1];

        String requestId ;

        try {
            ipAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            logger.error("Have error", e);
            ipAddress = null;
        }
        if (ipAddress != null) {
            ip = ipAddress.getHostAddress();
        }
        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        long heapFreeSize = Runtime.getRuntime().freeMemory();

        OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        double systemCpuLoad = operatingSystemMXBean.getSystemCpuLoad();
        double processCpuLoad = operatingSystemMXBean.getProcessCpuLoad();
        double freePhysicalMemory = operatingSystemMXBean.getFreePhysicalMemorySize();
        double freeSwapSpace = operatingSystemMXBean.getFreeSwapSpaceSize();
        List<String> arguments = runtimeMxBean.getInputArguments();

        // Get port
        for (String temp : arguments) {
            if (temp.trim().startsWith("-Dcom.viettel.mmserver.agent.port=")) {
                port = temp.substring(34).trim();
                path = ip + ":" + port + ":ThreadName=" + threadName;
                break;
            }
        }
        requestId = String.valueOf(startTime.getTime());
        if (logType != null && (("start_action").equalsIgnoreCase(logType) || ("start_connect").equalsIgnoreCase(logType))) {
            String startTimeStr = dateFormat.format(startTime);
            String logStart = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}",
                    new Object[]{logType, callId, systemName, startTimeStr, "", userName, ip, path, uri, param, className, "", "", clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace});
            logger.info(logStart);
        } else if (logType != null && (("end_action").equalsIgnoreCase(logType) || ("end_connect").equalsIgnoreCase(logType) || logType.startsWith("end"))) {
            endTime = new Date();
            duration = endTime.getTime() - startTime.getTime();
            String checkDuration = "";
            String endTimeStr = dateFormat.format(endTime);
            String startTimeStr = dateFormat.format(startTime);

            String logEnd = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}|{23}",
                    new Object[]{logType, callId, systemName, startTimeStr, endTimeStr, userName, ip, path, uri, param, className, duration.toString(), checkDuration, clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace, isOke});
            logger.info(logEnd);

            //neu giao dich vi pham kpi day ra file log rieng
            /*Long kpiValue = MemoryDataLoader.getConfigKpiMap().get(className + "_" + threadName);
            if (kpiValue != null) {
                kpiValue = kpiValue * 1000;
                if (duration.compareTo(kpiValue) > 0) {
                    loggerKpi.info(MessageFormat.format("|{0}|{1}|{2}|{3}", new Object[]{systemName.split(":")[0], threadName, className, duration.toString()}));
                }
            }*/
        } else {
            String startTimeStr = dateFormat.format(startTime);
            String logOthers = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}",
                    new Object[]{logType, callId, systemName, startTimeStr, "", userName, ip, path, uri, param, className, "", "", clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace});
            logger.info(logOthers);
        }
    }

    /**
     * writeLog
     * @param systemName
     * @param logType
     * @param threadName
     * @param className
     * @param uri
     * @param numOfProcessingRecord
     * @param numOfSuccessRecord
     * @param status
     * @param startTime
     */
    public static void writeLog(String systemName, String logType, String threadName, String className, String uri, String numOfProcessingRecord, String numOfSuccessRecord,
                                String status, Date startTime) {
        InetAddress ipAddress = null;
        String ip = "";
        Date endTime ;
        String userName = "Application";
        String path = "";
        String port ;
        String param = "";
        String clientKpiId = "";
        Long duration ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS", Locale.ENGLISH);

        String[] params = systemName.split(":");
        String callId = params[0];
        systemName = params[1];

        String requestId ;

        try {
            ipAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            logger.error("Have error", e);
            ipAddress = null;
        }
        if (ipAddress != null) {
            ip = ipAddress.getHostAddress();
        }
        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        long heapFreeSize = Runtime.getRuntime().freeMemory();

        OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        double systemCpuLoad = operatingSystemMXBean.getSystemCpuLoad();
        double processCpuLoad = operatingSystemMXBean.getProcessCpuLoad();
        double freePhysicalMemory = operatingSystemMXBean.getFreePhysicalMemorySize();
        double freeSwapSpace = operatingSystemMXBean.getFreeSwapSpaceSize();
        List<String> arguments = runtimeMxBean.getInputArguments();

        // Get port
        for (String temp : arguments) {
            if (temp.trim().startsWith("-Dcom.viettel.mmserver.agent.port=")) {
                port = temp.substring(34).trim();
                path = ip + ":" + port + ":ThreadName=" + threadName;
                break;
            }
        }
        requestId = String.valueOf(startTime.getTime());
        if (logType != null && ( ("start_action").equalsIgnoreCase(logType) ||  ("start_connect").equalsIgnoreCase(logType))) {
            String startTimeStr = dateFormat.format(startTime);
            String logStart = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}",
                    new Object[]{logType, callId, systemName, startTimeStr, "", userName, ip, path, uri, param, className, "", "", clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace});
            logger.info(logStart);
        } else if (logType != null && (("end_action").equalsIgnoreCase(logType) || ("end_connect").equalsIgnoreCase(logType) || logType.startsWith("end"))) {
            endTime = new Date();
            duration = endTime.getTime() - startTime.getTime();
            String checkDuration = "";
            String endTimeStr = dateFormat.format(endTime);
            String startTimeStr = dateFormat.format(startTime);

            String logEnd = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}",
                    new Object[]{logType, callId, systemName, startTimeStr, endTimeStr, userName, ip, path, uri, param, className, duration.toString(), checkDuration, clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace});
            logger.info(logEnd);

            //neu giao dich vi pham kpi day ra file log rieng
            /*Long kpiValue = MemoryDataLoader.getConfigKpiMap().get(className + "_" + threadName);
            if (kpiValue != null) {
                kpiValue = kpiValue * 1000;
                if (duration.compareTo(kpiValue) > 0) {
                    loggerKpi.info(MessageFormat.format("|{0}|{1}|{2}|{3}", new Object[]{systemName.split(":")[0], threadName, className, duration.toString()}));
                }
            }*/
        } else {
            String startTimeStr = dateFormat.format(startTime);
            String logOthers = MessageFormat.format("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}|{18}|{19}|{20}|{21}|{22}",
                    new Object[]{logType, callId, systemName, startTimeStr, "", userName, ip, path, uri, param, className, "", "", clientKpiId, requestId, numOfProcessingRecord, numOfSuccessRecord, status, heapFreeSize, systemCpuLoad, processCpuLoad, freePhysicalMemory, freeSwapSpace});
            logger.info(logOthers);
        }
    }
}