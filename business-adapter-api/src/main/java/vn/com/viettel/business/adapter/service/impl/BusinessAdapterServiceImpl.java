package vn.com.viettel.business.adapter.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.dto.BotParamDTO;
import vn.com.viettel.business.adapter.dto.BusinessAdapterDTO;
import vn.com.viettel.business.adapter.model.MapServiceParam;
import vn.com.viettel.business.adapter.repositories.MapServiceParamRepository;
import vn.com.viettel.business.adapter.service.BusinessAdapterService;

@Service
@Transactional
public class BusinessAdapterServiceImpl implements BusinessAdapterService {

	@Autowired
	private MapServiceParamRepository mapServiceParamRepository;

	@Autowired
	private static PropertiesConfig config;

	@Override
	public JSONObject getSoapResponse(vn.com.viettel.business.adapter.model.ServiceBO service,
			BusinessAdapterDTO input) {
		try {
			URL obj = new URL(service.getUrl());

			String soapRequest = getSoapRequest(service, input);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod(service.getMethod());
			con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

			if (service.getIsAuthen() == 1) {
				con.setRequestProperty(service.getUserName(), service.getUserNameValue());
				con.setRequestProperty(service.getPassWord(), service.getPassWordValue());
			}

			con.setDoOutput(true);

			// Set Timeout
			con.setConnectTimeout(Integer.parseInt(config.getTimeout()));

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(soapRequest);
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println("response:" + response.toString());
			String responseString = response.toString();
			String formatString = formatResponseString(responseString);

			JSONObject soapDatainJsonObject = XML.toJSONObject(formatString);
//			JSONObject soapDatainJsonObject = XML.toJSONObject(response.toString());

			return soapDatainJsonObject;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getSoapRequest(vn.com.viettel.business.adapter.model.ServiceBO service, BusinessAdapterDTO input) {
		String input_template = service.getTemplateInput();
		try {
			// REPLACE CÁC THAM SỐ ỨNG VỚI VÙNG DỮ LIỆU DATA_SENDER
			// Lấy danh sách các tham số tương ứng với sevice tương ứng vùng data_sender
			List<MapServiceParam> listMapSP_0 = new ArrayList<>();
			List<MapServiceParam> lMSP_0 = mapServiceParamRepository
					.findAllByServiceIdAndLocation(service.getServiceId(), 0L, 1L);
			if (lMSP_0 != null) {
				listMapSP_0 = lMSP_0;
			}

			// Lấy key key được truyền từ bot core
			JSONObject inputJson = new JSONObject(input);
			JSONObject sender_data = inputJson.getJSONObject("sender_data");
			JSONArray keys = sender_data.names();
			List<String> listKey = new ArrayList();
			for (int i = 0; i < keys.length(); ++i) {
				listKey.add(keys.getString(i));
			}
			// Replace các tham số trong input_template bởi các giá trị
			for (MapServiceParam item : listMapSP_0) {
				if (listKey.contains(item.getBotParam())) {
					input_template = input_template.replace("%" + item.getBusinessParam() + "%",
							sender_data.getString(item.getBotParam()));
				} else {
					input_template = input_template.replace("%" + item.getBusinessParam() + "%",
							item.getDefaultValue());
					System.out.println(input_template);
				}
			}

			// REPLACE CÁC THAM SỐ ỨNG VỚI VÙNG DỮ LIỆU PARAM
			// Lấy danh sách các tham số tương ứng với sevice tương ứng vùng param

			List<MapServiceParam> listMapSP_1 = new ArrayList<>();
			List<MapServiceParam> lMSP_1 = mapServiceParamRepository
					.findAllByServiceIdAndLocation(service.getServiceId(), 1L, 1L);
			if (lMSP_1 != null) {
				listMapSP_1 = lMSP_1;
			}

			// lấy list param được truyền từ bot core
			List<String> listParam = new ArrayList();
			for (BotParamDTO it : input.getParams()) {
				listParam.add(it.getName());
			}

			// Replace các tham số trong input_template bởi các giá trị
			for (MapServiceParam item : listMapSP_1) {
				if (listParam.contains(item.getBotParam())) {
					for (BotParamDTO itemm : input.getParams()) {
						if (item.getBotParam().equals(itemm.getName())) {
							input_template = input_template.replace("%" + item.getBusinessParam() + "%",
									itemm.getValue());
						}

					}
				} else {
					input_template = input_template.replace("%" + item.getBusinessParam() + "%",
							item.getDefaultValue());
				}
			}

			// REPLACE CÁC THAM SỐ Không có trên bot core
			List<MapServiceParam> listMapSP_2 = new ArrayList<>();
			List<MapServiceParam> lMSP_2 = mapServiceParamRepository
					.findAllByServiceIdAndLocation(service.getServiceId(), 2L, 1L);
			if (lMSP_2 != null) {
				listMapSP_2 = lMSP_2;
			}

			// Replace các tham số trong input_template bởi các giá trị
			for (MapServiceParam item : listMapSP_2) {
				input_template = input_template.replace("%" + item.getBusinessParam() + "%", item.getDefaultValue());
			}

		} catch (Exception e) {
			e.fillInStackTrace();
		}
		return input_template;
	}

	@Override
	public JSONObject getRestResponse(vn.com.viettel.business.adapter.model.ServiceBO service,
			BusinessAdapterDTO input) {

		try {

//			URL url = new URL("http://localhost:8084/api/business_adapter/runAPI");
			URL url = new URL(service.getUrl());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
//			conn.setRequestMethod("POST");
			conn.setRequestMethod(service.getMethod());
			conn.setRequestProperty("Content-Type", "application/json");
			// Set Timeout
			conn.setConnectTimeout(Integer.parseInt(config.getTimeout()));
			if (service.getIsAuthen() == 1) {
				conn.setRequestProperty(service.getUserName(), service.getUserNameValue());
				conn.setRequestProperty(service.getPassWord(), service.getPassWordValue());
			}

			String inputJson = getSoapRequest(service, input);

			OutputStream os = conn.getOutputStream();
			os.write(inputJson.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... ");
			StringBuffer response = new StringBuffer();
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				response.append(output);
			}
			JSONObject json = new JSONObject(response.toString());

			conn.disconnect();
			return json;

		} catch (MalformedURLException e) {

			e.printStackTrace();
			return null;

		} catch (IOException e) {

			e.printStackTrace();
			return null;
		}
	}

	String formatResponseString(String responseString) {
		// Get list tag
		List<String> listTag = getListTag(responseString);

		// Get string content
		List<String> coupleTagContentList = getCoupleTagContent(listTag);
		String contentString = responseString.split(listTag.get(0))[1].split(listTag.get(1))[0];

		// Clear tag here
		return contentString;
	}

//	String formatTag(String tag) {
//		String format = "";
//		String endtag = tag.split(":")[1];
//		tag.substring(0, 2);
//		if (tag.substring(0, 2).equals("</")) {
//			format = "</" + endtag;
//		} else {
//			format = "<" + endtag;
//		}
//		return format;
//	}

	List<String> getListTag(String string) {
		List<String> listTag = new ArrayList<String>();

		if (string != null && string.length() > 0) {
			int start = 0;
			int end = 0;
			for (int i = 0; i < string.length(); i++) {
				if (string.charAt(i) == '<') {
					start = i;
				}

				if (string.charAt(i) == '>') {
					end = i;
					String tag = string.substring(start, end + 1);
					listTag.add(tag);
				}

			}
		}
		return listTag;
	}

	List<String> getCoupleTagContent(List<String> listTag) {
		List<String> coupleTagContent = new ArrayList<>();
		int number = 0;
		for (int i = 0; i < listTag.size(); i++) {
			if (listTag.get(i).toLowerCase().contains("body")) {
				number = number + 1;
				if (number == 1) {
					coupleTagContent.add(listTag.get(i + 1));
				}
				if (number == 2) {
					coupleTagContent.add(listTag.get(i - 1));
				}
			}
		}
		return coupleTagContent;
	}

}
