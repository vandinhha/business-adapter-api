package vn.com.viettel.business.adapter.repositories;


import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.model.MapTemplateService;



@Repository
@Transactional
public class MapTemplateServiceRepositoryImpl implements MapTemplateServiceRepository {
	@PersistenceContext
    EntityManager entityManager;
	@Override
	public List<MapTemplateService> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MapTemplateService> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MapTemplateService> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapTemplateService> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends MapTemplateService> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<MapTemplateService> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MapTemplateService getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapTemplateService> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapTemplateService> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<MapTemplateService> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  MapTemplateService  save(MapTemplateService entity) {
		if (entity.getMapTemplateServiceId() == null) {
		      entityManager.persist(entity);
		      return entity;
		    } else {
		      return entityManager.merge(entity);
		    }
	}

	@Override
	public Optional<MapTemplateService> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(MapTemplateService entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends MapTemplateService> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends MapTemplateService> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapTemplateService> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends MapTemplateService> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends MapTemplateService> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<MapTemplateService> findByTemplateId(Long templateId) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	//@Cacheable(value = "findAllMapTemplateServiceByTemplateId")
	public List<MapTemplateService> findAllByTemplateId(Long templateId, Long status) {
		 Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_template_service as tm " +
	                "WHERE tm.template_id =:templateId and tm.status =:status order by tm.ord", MapTemplateService.class);
	        query.setParameter("templateId", templateId);
	        query.setParameter("status", status);
	        return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public MapTemplateService findByMapTemplateServiceId(Long id) {
		 Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_template_service as tm " +
	                "WHERE tm.map_template_service_id =:id AND status=:status", MapTemplateService.class);
	        query.setParameter("id",id);
	        query.setParameter("status",1L);
	        List<MapTemplateService> lst= query.getResultList();
	        if(lst.size()>0){
	        	return lst.get(0);
	        }
	        return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MapTemplateService findByTemplateAndService(Long templateId, Long serviceId) {
		Query query = entityManager.createNativeQuery("SELECT tm.* FROM map_template_service as tm " +
                "WHERE tm.service_id =:serviceId AND template_id=:templateId AND status=:status", MapTemplateService.class);
        query.setParameter("serviceId",serviceId);
        query.setParameter("templateId",templateId);
        query.setParameter("status",1L);
        List<MapTemplateService> lst= query.getResultList();
        if(lst.size()>0){
        	return lst.get(0);
        }
        return null;
	}

	
}
