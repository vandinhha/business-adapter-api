/*
 * Copyright (C) 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package vn.com.viettel.business.adapter.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class SocketClient {
	public static void main(String[] args) {
		// Địa chỉ máy chủ.
	       final String serverHost = "localhost";
	 
	       Socket socketOfClient = null;
	       BufferedWriter os = null;
	       BufferedReader is = null;
	 
	       try {
	           // Gửi yêu cầu kết nối tới Server đang lắng nghe
	           // trên máy 'localhost' cổng 9999.
	           socketOfClient = new Socket(serverHost, 9098);
	 
	           // Tạo luồng đầu ra tại client (Gửi dữ liệu tới server)
	           os = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
	 
	           // Luồng đầu vào tại Client (Nhận dữ liệu từ server).
	           is = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
	 
	       } catch (UnknownHostException e) {
	           System.err.println("Don't know about host " + serverHost);
	       } catch (IOException e) {
	           System.err.println("Couldn't get I/O for the connection to " + serverHost);
	       }
	 
	       try {
	           // Ghi dữ liệu vào luồng đầu ra của Socket tại Client.
	           os.write("IMPORT 21;");
	           os.newLine(); // kết thúc dòng
	           os.flush();  // đẩy dữ liệu đi.
	
	           // Đọc dữ liệu trả lời từ phía server
	           // Bằng cách đọc luồng đầu vào của Socket tại Client.
	           String responseLine = is.readLine();
	           System.out.println("Server: " + responseLine);
	           os.close();
	           is.close();
	           socketOfClient.close();
	       } catch (UnknownHostException e) {
	           System.err.println("Trying to connect to unknown host: " + e);
	       } catch (IOException e) {
	           System.err.println("IOException:  " + e);
	       }
	}

}    
