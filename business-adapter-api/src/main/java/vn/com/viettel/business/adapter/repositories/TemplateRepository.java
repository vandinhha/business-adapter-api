package vn.com.viettel.business.adapter.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.business.adapter.model.Template;;

public interface TemplateRepository extends JpaRepository<Template, Long> {
	
	List<Template> findByTemplateCode(String templateCode, Long status);
	public Template findByTemplateId(Long id);
	
}

