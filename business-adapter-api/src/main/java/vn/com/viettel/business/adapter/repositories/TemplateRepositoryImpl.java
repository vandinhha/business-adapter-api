package vn.com.viettel.business.adapter.repositories;


import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.model.Template;



@Repository
@Transactional
public class TemplateRepositoryImpl implements TemplateRepository {
	@PersistenceContext
    EntityManager entityManager;
	@Override
	public List<Template> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Template> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Template> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Template> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Template> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<Template> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Template getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Template> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Template> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Template> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Template save(Template entity) {
		if (entity.getTemplateId() == null) {
		      entityManager.persist(entity);
		      return entity;
		    } else {
		      Template obj= entityManager.merge(entity);
		       return obj;
		    }
	}


	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		
	}

	@Override
	public void delete(Template entity) {
		entityManager.remove(entity);
		
	}

	@Override
	public void deleteAll(Iterable<? extends Template> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Template> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Template> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Template> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Template> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	// @Cacheable(value = "findTemplateByTemplateCode")
	public List<Template> findByTemplateCode(String templateCode, Long status) {
		 Query query = entityManager.createNativeQuery("SELECT tm.* FROM template as tm " +
	                "WHERE tm.template_code =:templateCode and tm.status =:status", Template.class);
	        query.setParameter("templateCode", templateCode);
	        query.setParameter("status", status);
	        return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Template findByTemplateId(Long id) {
			 Query query = entityManager.createNativeQuery("SELECT tm.* FROM template as tm " +
		                "WHERE tm.template_id =:id AND status=:status", Template.class);
		        query.setParameter("id",id);
		        query.setParameter("status",1L);
		        List<Template> lst= query.getResultList();
		        if(lst.size()>0){
		        	return lst.get(0);
		        }
		        return null;
	}

	@Override
	public Optional<Template> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
