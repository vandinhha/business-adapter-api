package vn.com.viettel.business.adapter.service;

import org.springframework.stereotype.Service;

public interface AuthenticateService {

	public boolean checkAuthen(String userName, String password, String ip);
}
