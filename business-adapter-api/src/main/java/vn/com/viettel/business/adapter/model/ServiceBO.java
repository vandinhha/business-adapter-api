package vn.com.viettel.business.adapter.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.dto.ServiceDTO;

@Entity
@Table(name = "service")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceBO implements Serializable, Cloneable {

	private static final long serialVersionUID = 274475190788082021L;

	@Id
	@Column(name = "service_id", nullable = false)
	@GeneratedValue(strategy = IDENTITY)
	private Long serviceId;

	@Column(name = "service_code", nullable = false)
	private String serviceCode;

	@Column(name = "url", nullable = false)
	private String url;

	@Column(name = "template_input", nullable = false)
	private String templateInput;

	@Column(name = "wsdl", nullable = false)
	private String wsdl;

	@Column(name = "template_output", nullable = false)
	private String templateOutput;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String passWord;

	@Column(name = "status")
	private Long status;

	@Column(name = "is_return")
	private Long isReturn;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "create_user")
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Column(name = "method")
	private String method;

	@Column(name = "type")
	private Long type;
		
	@Column(name = "user_name_value")
	private String userNameValue;

	@Column(name = "password_value")
	private String passWordValue;

	@Column(name = "is_authen")
	private Long isAuthen;
	
	
	

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTemplateInput() {
		return templateInput;
	}

	public void setTemplateInput(String templateInput) {
		this.templateInput = templateInput;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public String getTemplateOutput() {
		return templateOutput;
	}

	public void setTemplateOutput(String templateOutput) {
		this.templateOutput = templateOutput;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public Long getIsReturn() {
		return isReturn;
	}

	public void setIsReturn(Long isReturn) {
		this.isReturn = isReturn;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getUserNameValue() {
		return userNameValue;
	}

	public void setUserNameValue(String userNameValue) {
		this.userNameValue = userNameValue;
	}

	public String getPassWordValue() {
		return passWordValue;
	}

	public void setPassWordValue(String passWordValue) {
		this.passWordValue = passWordValue;
	}

	public Long getIsAuthen() {
		return isAuthen;
	}

	public void setIsAuthen(Long isAuthen) {
		this.isAuthen = isAuthen;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	public ServiceDTO toDTO() {
		ServiceDTO ret = new ServiceDTO();
		ret.setService_id(this.serviceId.toString());
		ret.setServiceCode(this.serviceCode);
		ret.setUrl(this.url);
		ret.setWsdl(this.wsdl);
		ret.setTemplateInput(this.templateInput);
		ret.setTemplateOutput(this.templateOutput);
		ret.setIsReturn(this.isReturn.toString());
		ret.setUserName(this.userName);
		ret.setPassWord(this.passWord);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		ret.setMethod(this.method);
		ret.setType(this.type);
		ret.setUserNameValue(userNameValue);
		ret.setPassWordValue(passWordValue);
		ret.setIsAuthen(isAuthen);
		return ret;
	}

}
