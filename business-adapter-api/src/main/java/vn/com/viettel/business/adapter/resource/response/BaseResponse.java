package vn.com.viettel.business.adapter.resource.response;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String CODE_SUCCESS = "00";
	public static final String CODE_ERROR = "01";
	public static final String CODE_UNAUTHORIZED = "403";
	public static final String CODE_SYSTEM_ERR = "02";
	public static final String CODE_CONNECTION_MAX = "03";
	
	private String errorCode;
	private String description;

	
	private T data;
	
	public BaseResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	

}
