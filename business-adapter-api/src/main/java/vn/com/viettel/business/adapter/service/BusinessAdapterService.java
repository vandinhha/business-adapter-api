package vn.com.viettel.business.adapter.service;

import org.json.JSONObject;

import vn.com.viettel.business.adapter.dto.BusinessAdapterDTO;
import vn.com.viettel.business.adapter.model.ServiceBO;

public interface BusinessAdapterService {
	
	public String getSoapRequest(ServiceBO service, BusinessAdapterDTO input);
	public JSONObject getSoapResponse(ServiceBO service, BusinessAdapterDTO input);
	public JSONObject getRestResponse(ServiceBO service, BusinessAdapterDTO input);
	
}
