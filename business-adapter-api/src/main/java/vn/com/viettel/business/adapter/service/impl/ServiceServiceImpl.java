package vn.com.viettel.business.adapter.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.config.PropertiesConfig;
import vn.com.viettel.business.adapter.dto.ServiceDTO;
import vn.com.viettel.business.adapter.model.ServiceBO;
import vn.com.viettel.business.adapter.repositories.ServiceRepository;
import vn.com.viettel.business.adapter.service.ServiceService;
import vn.com.viettel.business.adapter.utils.DataUtil;

@Service
@Transactional
public class ServiceServiceImpl implements ServiceService{

	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	private PropertiesConfig config;
	
	public List<ServiceBO> findByServiceCode(String templateCode, Long status){
		return serviceRepository.findByServiceCode(templateCode, status);
	}
	@Override
	public List<Long> createService(List<ServiceDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getServiceCode())){
					throw new Exception(config.getCodeReq());
				}
				if(StringUtils.isEmpty(list.get(i).getUrl())){
					throw new Exception(config.getUrlReq());
				}
				if(StringUtils.isEmpty(list.get(i).getWsdl())){
					throw new Exception(config.getWsdlReq());
				}
				if(StringUtils.isEmpty(list.get(i).getTemplateInput())){
					throw new Exception(config.getTemplateInputReq());
				}
				if(StringUtils.isEmpty(list.get(i).getTemplateOutput())){
					throw new Exception(config.getTemplateOuputReq());
				}
				if(StringUtils.isEmpty(list.get(i).getIsReturn())){
					throw new Exception(config.getIsReturnReq());
				}
				if(!DataUtil.checkLong(list.get(i).getIsReturn())){
					throw new Exception(config.getValidateError());
				}
				if(findByServiceCode(list.get(i).getServiceCode(), 1L)!=null&&findByServiceCode(list.get(i).getServiceCode(), 1L).size()>0) {
					throw new Exception(config.getCodeDuplicate());
				}
				list.get(i).setStatus(1L);
				list.get(i).setCreateDate(new Date());
				list.get(i).setCreateUser(config.getAppUserName());
				Long id =serviceRepository.save(list.get(i).toBO()).getServiceId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> updateService(List<ServiceDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getService_id())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getService_id())) {
					throw new Exception(config.getValidateError());
				}
				ServiceBO service = serviceRepository.getOne(Long.parseLong(list.get(i).getService_id()),1L);
				if(service==null) {
					throw new Exception(config.getIdNotExits());
				}
				if(StringUtils.isEmpty(list.get(i).getServiceCode())){
					throw new Exception(config.getCodeReq());
				}
				if(StringUtils.isEmpty(list.get(i).getUrl())){
					throw new Exception(config.getUrlReq());
				}
				if(StringUtils.isEmpty(list.get(i).getWsdl())){
					throw new Exception(config.getWsdlReq());
				}
				if(StringUtils.isEmpty(list.get(i).getTemplateInput())){
					throw new Exception(config.getTemplateInputReq());
				}
				if(StringUtils.isEmpty(list.get(i).getTemplateOutput())){
					throw new Exception(config.getTemplateOuputReq());
				}
				if(StringUtils.isEmpty(list.get(i).getIsReturn())){
					throw new Exception(config.getIsReturnReq());
				}
				if(!DataUtil.checkLong(list.get(i).getIsReturn())){
					throw new Exception(config.getValidateError());
				}
				if(findByServiceCode(list.get(i).getServiceCode(), 1L)!=null&&findByServiceCode(list.get(i).getServiceCode(), 1L).size()>0&&Long.parseLong(list.get(i).getService_id())!=findByServiceCode(list.get(i).getServiceCode(), 1L).get(0).getServiceId()) {
					throw new Exception(config.getCodeDuplicate());
				}
				list.get(i).setUpdateDate(new Date());
				list.get(i).setStatus(service.getStatus());
				list.get(i).setUpdateUser(config.getAppUserName());
				list.get(i).setCreateUser(service.getCreateUser());
				list.get(i).setCreateDate(service.getCreateDate());
				Long id =serviceRepository.save(list.get(i).toBO()).getServiceId();
				listId.add(id);
			}
		}
		return listId;
	}

	@Override
	public List<Long> deleteService(List<ServiceDTO> list) throws Exception{
		List<Long> listId=new ArrayList<Long>();
		if(list!=null&&list.size()>0) {
			for(int i=0;i<list.size();i++) {
				if(StringUtils.isEmpty(list.get(i).getService_id())) {
					throw new Exception(config.getIdReq());
				}
				if(!DataUtil.checkLong(list.get(i).getService_id())) {
					throw new Exception(config.getValidateError());
				}
				ServiceBO service = serviceRepository.getOne(Long.parseLong(list.get(i).getService_id()),1L);
				if(service==null) {
					throw new Exception(config.getIdNotExits());
				}
				service.setStatus(0L);
				serviceRepository.save(service);
				listId.add(Long.parseLong(list.get(i).getService_id()));
			}
		}
		return listId;
	}

}
