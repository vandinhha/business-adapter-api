package vn.com.viettel.business.adapter.constant;

public final class AppConst {
	public static enum LogAction {
	   ADD("ADD"), UPDATE("UPDATE"), CHANGE_STATE("CHANGE_STATE"), READ("READ"), DELETE("DELETE");
	   private final String text;

	    /**
	     * @param text
	     */
	   LogAction(final String text) {
	        this.text = text;
	    }

	    /* (non-Javadoc)
	     * @see java.lang.Enum#toString()
	     */
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	public static final String SYSTEM_NAME = "Business Adapter API";
}
