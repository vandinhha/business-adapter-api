package vn.com.viettel.business.adapter.model;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.dto.MapTemplateServiceDTO;

@Entity
@Table(name = "map_template_service")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MapTemplateService implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
	@Id
	@Column(name = "map_template_service_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long mapTemplateServiceId;

	@Column(name = "template_id", nullable = false)
	private Long templateId;

	@Column(name = "service_id", nullable = false)
	private Long serviceId;
		
	@Column(name = "status")
	private Long status;
	
	@Column(name = "ord")
	private Long order;

	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "create_user")
	private String createUser;
	
	@Column(name = "update_user")
	private String updateUser;
	

	
	
	public Long getTemplateId() {
		return templateId;
	}




	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	



	public Long getMapTemplateServiceId() {
		return mapTemplateServiceId;
	}




	public void setMapTemplateServiceId(Long mapTemplateServiceId) {
		this.mapTemplateServiceId = mapTemplateServiceId;
	}




	public Long getServiceId() {
		return serviceId;
	}




	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}




	public Long getOrder() {
		return order;
	}




	public void setOrder(Long order) {
		this.order = order;
	}


	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public MapTemplateServiceDTO toDTO() {
		MapTemplateServiceDTO ret  = new MapTemplateServiceDTO();
		ret.setMapTemplateServiceId(this.mapTemplateServiceId.toString());
		ret.setServiceId(this.serviceId.toString());
		ret.setTemplateId(this.templateId.toString());
		ret.setOrder(this.order.toString());
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateDate(this.updateDate);
		return ret;
	}
	
	
}
