package vn.com.viettel.business.adapter.model;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.dto.ParamDTO;

@Entity
@Table(name = "param")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Param implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_sequence")
//    @SequenceGenerator(name = "user_sequence", sequenceName = "PERMIT_USER_SEQ")
	@Id
	@Column(name = "param_id", nullable = false)
	@GeneratedValue(strategy=IDENTITY)
	private Long paramId;

	@Column(name = "business_param", nullable = false)
	private String businessParam;

	@Column(name = "default_value", nullable = false)
	private String defaultValue;
		
	@Column(name = "status")
	private Long status;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "create_user")
	private String createUser;
	
	@Column(name = "update_user")
	private String updateUser;
	

	
	
	


	public Long getParamId() {
		return paramId;
	}




	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}




	public String getBusinessParam() {
		return businessParam;
	}




	public void setBusinessParam(String businessParam) {
		this.businessParam = businessParam;
	}




	public String getDefaultValue() {
		return defaultValue;
	}




	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public ParamDTO toDTO() {
		ParamDTO ret  = new ParamDTO();
		ret.setParamId(this.paramId);
		ret.setBusinessParam(this.businessParam);
		ret.setDefaultValue(this.defaultValue);
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateDate(this.updateDate);
		return ret;
	}
	
	
}
