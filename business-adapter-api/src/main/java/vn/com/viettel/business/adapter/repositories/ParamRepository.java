package vn.com.viettel.business.adapter.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.viettel.business.adapter.model.Param;

public interface ParamRepository extends JpaRepository<Param, Long> {
	Param getOne(String code, Long status);	
}
