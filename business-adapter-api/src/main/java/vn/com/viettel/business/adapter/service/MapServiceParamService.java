package vn.com.viettel.business.adapter.service;

import java.util.List;

import vn.com.viettel.business.adapter.dto.MapServiceParamDTO;

public interface MapServiceParamService {
	List<Long> createMapServiceParam(List<MapServiceParamDTO> list) throws Exception;
	List<Long> updateMapServiceParam(List<MapServiceParamDTO> list) throws Exception;
	List<Long> deleteMapServiceParam(List<MapServiceParamDTO> list) throws Exception;
}
