package vn.com.viettel.business.adapter.dto;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import vn.com.viettel.business.adapter.model.MapTemplateService;
import vn.com.viettel.business.adapter.utils.JsonDateSerializerDate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MapTemplateServiceDTO {

	private String mapTemplateServiceId;

	private String templateId;

	private String serviceId;

	private Long status;

	private String order;

	@JsonSerialize(using = JsonDateSerializerDate.class)
	private Date createDate;
	@JsonSerialize(using = JsonDateSerializerDate.class)
	private Date updateDate;

	private String createUser;
	private String updateUser;

	

	public String getMapTemplateServiceId() {
		return mapTemplateServiceId;
	}

	public void setMapTemplateServiceId(String mapTemplateServiceId) {
		this.mapTemplateServiceId = mapTemplateServiceId;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public MapTemplateService toBO() {
		MapTemplateService ret = new MapTemplateService();
		if(this.mapTemplateServiceId!=null) {
		ret.setMapTemplateServiceId(Long.parseLong(this.mapTemplateServiceId));
		}
		ret.setServiceId(Long.parseLong(this.serviceId));
		ret.setTemplateId(Long.parseLong(this.templateId));
		ret.setOrder(Long.parseLong(this.order));
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateDate(this.updateDate);
		return ret;
	}

}
