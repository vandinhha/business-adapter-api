package vn.com.viettel.business.adapter.dto;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.business.adapter.model.MapServiceParam;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MapServiceParamDTO{

	
	
	private String mapServiceParamId;
	private String serviceId;
	private String businessParam;
	private String defaultValue;
	private String location;
	private String botParam;
	private Long status;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;
	



	public String getBusinessParam() {
		return businessParam;
	}




	public void setBusinessParam(String businessParam) {
		this.businessParam = businessParam;
	}




	public String getDefaultValue() {
		return defaultValue;
	}




	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}





	public String getBotParam() {
		return botParam;
	}




	public void setBotParam(String botParam) {
		this.botParam = botParam;
	}




	public Long getStatus() {
		return status;
	}




	public void setStatus(Long status) {
		this.status = status;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public Date getUpdateDate() {
		return updateDate;
	}




	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	public String getCreateUser() {
		return createUser;
	}




	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}




	public String getUpdateUser() {
		return updateUser;
	}




	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	public String getMapServiceParamId() {
		return mapServiceParamId;
	}




	public void setMapServiceParamId(String mapServiceParamId) {
		this.mapServiceParamId = mapServiceParamId;
	}




	public String getServiceId() {
		return serviceId;
	}




	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}




	public String getLocation() {
		return location;
	}




	public void setLocation(String location) {
		this.location = location;
	}




	public MapServiceParam toBO() {
		MapServiceParam ret  = new MapServiceParam();
		if(this.mapServiceParamId!=null) {
		ret.setMapServiceParamId(Long.parseLong(this.mapServiceParamId));
		}
		ret.setServiceId(Long.parseLong(this.serviceId));
		ret.setBusinessParam(this.businessParam);
		ret.setBotParam(this.botParam);
		ret.setDefaultValue(this.defaultValue);
		ret.setLocation(Long.parseLong(this.location));
		ret.setStatus(this.status);
		ret.setCreateDate(this.createDate);
		ret.setUpdateDate(this.updateDate);
		ret.setCreateUser(this.createUser);
		ret.setUpdateUser(this.updateUser);
		return ret;
	}
	
	
}
