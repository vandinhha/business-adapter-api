package vn.com.viettel.business.adapter.utils;

import java.util.List;

import vn.com.viettel.business.adapter.resource.response.BaseResponse;

public class ResponseUtils {

	public static <T> BaseResponse<T> createResponse(String code, String description, T t){
		BaseResponse<T> base = new BaseResponse<>();
		base.setData(t);
		base.setDescription(description);
		base.setErrorCode(code);
		return base;
	}
	public static BaseResponse<List<Long>> createResponseListId(String code, String description, List<Long> list){
		BaseResponse<List<Long>> base = new BaseResponse<>();
		base.setData(list);
		base.setDescription(description);
		base.setErrorCode(code);
		return base;
	}
	
}
