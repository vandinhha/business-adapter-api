package vn.com.viettel.business.adapter.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class JarFileUtils {
	public static void main(String[] args) {

		File fileOrDir = new File(
				"/media/tamdx/work/git_public/business-adapter-api/m2");
		JarFileUtils.CopyAllJarFile(fileOrDir);

	}

	public static void CopyAllJarFile(final File fileOrDir) {
		
        // check xem fileOrDir là file hay dir
        if (fileOrDir.isDirectory()) {
            // Lấy danh sách file and dir con
            final File[] children = fileOrDir.listFiles();
            if (children == null) {
                return;
            }

            for (final File each : children) {
                // gọi lại hàm CopyAllJarFile()
            	CopyAllJarFile(each);
            }
        } else {
        	
            // Nếu là file 
        	String target = "/media/tamdx/work/git_public/business-adapter-api/business-adapter-api/lib";
        	String sourceFilePath = fileOrDir.getAbsolutePath();
            System.out.println(sourceFilePath);
            String extend = sourceFilePath.split("\\.")[sourceFilePath.split("\\.").length-1];
            
            //Kiểm tra là file.jar
            if(extend.contains("jar")) {
            	String fileName = sourceFilePath.split("\\\\")[sourceFilePath.split("\\\\").length-1];
            	String destFilePath = target + fileName;
            	File destFile= new File(destFilePath);
            	JarFileUtils.copyFileUsingStream(fileOrDir, destFile);     	
            }           
        }
    }

	private static void copyFileUsingStream(File source, File dest) {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} catch (Exception ex) {
			System.out.println("Unable to copy file:" + ex.getMessage());
		} finally {
			try {
				is.close();
				os.close();
			} catch (Exception ex) {
			}
		}
	}
}
