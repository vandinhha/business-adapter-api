package vn.com.viettel.business.adapter.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.business.adapter.model.ServiceBO;

@Repository
@Transactional
public class ServiceRepositoryImpl implements ServiceRepository {
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<ServiceBO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ServiceBO> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ServiceBO> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ServiceBO> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub

	}

	@Override
	public <S extends ServiceBO> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<ServiceBO> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	//@Cacheable(value = "getOneServiceByServiceId")
	public ServiceBO getOne(Long id, Long status) {
		Query query = entityManager.createNativeQuery(
				"SELECT tm.* FROM service as tm " + "WHERE tm.service_id =:serviceId and tm.status =:status", ServiceBO.class);
		query.setParameter("serviceId", id);
		query.setParameter("status", status);
		List<ServiceBO> lst = query.getResultList();
		if (lst.size() > 0) {
			return lst.get(0);
		}
		return null;
	}

	@Override
	public <S extends ServiceBO> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ServiceBO> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<ServiceBO> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceBO save(ServiceBO entity) {
		if (entity.getServiceId() == null) {
		      entityManager.persist(entity);
		      return entity;
		    } else {
		      return entityManager.merge(entity);
		    }
	}

	@Override
	public Optional<ServiceBO> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(ServiceBO entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll(Iterable<? extends ServiceBO> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public <S extends ServiceBO> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ServiceBO> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ServiceBO> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends ServiceBO> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<ServiceBO> findAllByTemplateId(String templateId) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<ServiceBO> findByServiceId(Long serviceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceBO getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ServiceBO> findByServiceCode(String serviceCode, Long status) {
			 Query query = entityManager.createNativeQuery("SELECT tm.* FROM service as tm " +
		                "WHERE tm.service_code =:serviceCode and tm.status =:status", ServiceBO.class);
		        query.setParameter("serviceCode", serviceCode);
		        query.setParameter("status", status);
		        return query.getResultList();
	}



}
