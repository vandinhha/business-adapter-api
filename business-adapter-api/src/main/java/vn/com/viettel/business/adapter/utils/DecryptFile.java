package vn.com.viettel.business.adapter.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class DecryptFile {

	public static void main(String[] args) {
		List<String> fileNames = java.util.Arrays
				.asList(new String[] { "/Users/phamkyit/Desktop/filesss/product-catalog-api-0.0.1-SNAPSHOT.part1.rar",
						"/Users/phamkyit/Desktop/filesss/product-catalog-api-0.0.1-SNAPSHOT.part2.rar",
						"/Users/phamkyit/Desktop/filesss/product-catalog-api-0.0.1-SNAPSHOT.part3.rar",
						"/Users/phamkyit/Desktop/filesss/product-catalog-api-0.0.1-SNAPSHOT.part4.rar",
						"/Users/phamkyit/Desktop/filesss/product-catalog-api-0.0.1-SNAPSHOT.part5.rar" });
		for (int i = 0; i < fileNames.size(); i++) {
			try {
				System.out.println("Processing: " + fileNames.get(i));
				RandomAccessFile raf = new RandomAccessFile(fileNames.get(i), "rw");
				byte[] data = new byte[(int) (raf.length())];
				raf.read(data);
				raf.close();
				byte[] dataRemove = Arrays.copyOfRange(data, 5, data.length);
				try {
		            Path path = Paths.get(fileNames.get(i));
		            Files.write(path, dataRemove);
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
//				for (int j = 0; j < 5; j++)
//					raf.write(0x20);
				raf.close();
				System.out.println("OK: " + fileNames.get(i));
			} catch (FileNotFoundException ex) {
				System.out.println("File not found!");
			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}

	}
}
